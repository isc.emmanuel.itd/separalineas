# -*- coding: utf-8 -*-
import polyskel as polyskel
import numpy as np
from time import time as t
from arcpy import SpatialReference as sPt, CreateFeatureclass_management as cFc, CreateFeatureDataset_management as cFd, MakeFeatureLayer_management as mF
from arcpy.management import FeatureVerticesToPoints as fV, AddField as aF
from arcpy.da import SearchCursor as sC, InsertCursor as iC


def getPend(p1,p2):
    try:
        return (p2[0]-p1[0])/(p2[1]-p1[1])
    except Exception as e:
        print(p1,p2)
        return 0


class Convertir:
    def __init__(self,_s,_d,_v,_g,_l):
        print(_g)
        self._nom = _s
        self.sPt = sPt(6372)
        self._dSet = cFd(_g,"tmp",self.sPt)
        self._feat = _l
        self.sF = mF(_l,"ori")
        self.resulFeat = cFc(self._dSet,"resultado","POLYGON",self.sF,"DISABLED","DISABLED",self.sPt)
        self._nom_l = None
        self._nom_c= None
        self._dist = _d
        self.m = 0
        self.id=0
        self.vtx= _v
        fV("ori","tmp/nodos","ALL")
        self.objGeom = {}
        for i in [id[0] for id in sC("tmp/nodos",["id_tmp"],sql_clause=("DISTINCT id_tmp","ORDER BY id_tmp"))]:
            self.objGeom[i]= [n[1] for n in sC("tmp/nodos",["id_tmp","SHAPE@XY"]) if n[0]==i]
    def getId(s):
        s.id+=1
        return s.id
    def getGeom(s,k):
        return s.objGeom[k]
    def setGeom(s,_g,k):
        s.objGeom[k]=_g
    def setNom(s,n):
        s._nom = n
    def getNom(s):
        return s._nom
    def getNomL(s):
        return s._nom_l
    def gK (_s):
        return list(_s.objGeom.keys()) 


    def Skeletor(_s,_n):
        def juntar(c):
            _juntos.append(c[0])
            _juntos.append(c[1])
        def unir(_linCen,i):
            _idx=0   
            def vali_Ang(_a):
                return True if 240<_a<297 or 75<_a<105 else False 
            try:
                _idx = _juntos.index(_linCen[i])
            except Exception as e:
                return False
            _vtN = _juntos[_idx+1]  if _idx%2 == 0  else _juntos[_idx-1]
            if i==0:
                _linCen.reverse()
            _linCen.append(_vtN)
            arr = np.asarray(_linCen)
            if len(arr)>2:
                _ang = _s._angles_(arr,inside=True,in_deg=True)
                return vali_Ang(_ang[1])
            return False
        
        objTmp = {"_liniukis":[],"_centra":[]}
        print("[info] Generando Líneas auxiliares")
        for i in _s.gK():
            _lines,_centra,_juntos = [],[],[]
            ini_skel = t()
            print("Elemento %d de %d..." % (i,len(_s.gK())))
            skeleton = polyskel.skeletonize(_s.getGeom(i))
            print("Tiempo: %.3f" % (t()-ini_skel) )
            _pol = _s.getGeom(i)
            for arc in skeleton:
                for sink in arc.sinks:
                    if  _s._dist*0.5  <  arc.height   <  _s._dist :
                        if (arc.source.x, arc.source.y) in _pol or (sink.x, sink.y) in _pol:
                            _lines.append([(arc.source.x, arc.source.y),(sink.x, sink.y)])
                        else:
                            _centra.append([(arc.source.x, arc.source.y),(sink.x, sink.y)])
            [juntar(c) for c in _centra]
            _liniukis = [l for l in _lines  if unir(l,0) or unir(l,1)]
            objTmp["_liniukis"].append(_liniukis)
            objTmp["_centra"].append(_centra)
        return objTmp
    
    def _angles_(_s,a, inside, in_deg):  
        def _x_(a):       
            ba = a - np.concatenate((a[-1, None], a[:-1]), axis=0)
            bc = a - np.concatenate((a[1:], a[0, None]), axis=0)
            return np.cross(ba, bc), ba, bc 
        cr, ba, bc = _x_(a)
        dt = np.einsum('ij,ij->i', ba, bc)    
        ang = np.arctan2(cr, dt) 
        TwoPI = np.pi * 2
        if inside:
            angles = np.where(ang < 0, ang + TwoPI, ang)
        else:
            angles = np.where(ang > 0, TwoPI - ang, ang)
        if in_deg:
            angles = np.degrees(angles)
        return angles

    def separar(_s):
        def outLine(_ar):
            return (False if _ar['y'][0] < _ar['y'][1] < _ar['y'][2] or _ar['y'][0] >_ar['y'][1] > _ar['y'][2]  else True )  if _ar['x'][0] == _ar['x'][1] == _ar['x'][2] else (False if _ar['x'][0] < _ar['x'][1] < _ar['x'][2] or _ar['x'][0] > _ar['x'][1] > _ar['x'][2] else True)
        def Dista(_l):
            _dista = ((_l['_coo'][1][0]-_l['_coo'][0][0])**2+(_l['_coo'][1][1]-_l['_coo'][0][1])**2)**0.5
            return _dista
        
        res = iC("tmp/resultado",["OID@","SHAPE@"])
        for nP in _s.gK():
            _pol = _s.getGeom(nP)
            _lin = [{"id":n[0],"_coo": n[1].__geo_interface__['coordinates'][0],"nP":n[2]} for n in sC("tmp/%s" %_s._nom_l,["OID@","SHAPE@","numPol"])]
            aMover={"ori":[],"nuevo":[]}
            for l in _lin:
                if l["nP"]==nP:
                    x1,y1 = l['_coo'][0][0],l['_coo'][0][1]
                    x2,y2 = l['_coo'][1][0],l['_coo'][1][1]
                    xd,yd = (x1,y1) if (x1,y1) in _pol else (x2,y2)
                    v1,v2 = x2-x1,y2-y1
                    xmas= xd+_s._dist
                    ymas = ((v2*xmas) -(v2*x1)+(v1*y1))/v1
                    xmen= xd-_s._dist
                    ymen = ((v2*xmen) -(v2*x1)+(v1*y1))/v1
                    dist = ((xd-xmas)**2+(yd-ymas)**2)**0.5
                    _obj={ }
                    prop=dist/(_s._dist/2)
                    dis1 = [(yd-ymas)/prop,_s._dist/prop]            
                    _pos = [ xd+dis1[1] , yd-dis1[0]]
                    _obj["x"],_obj["y"]=[x1,_pos[0],x2],[y1,_pos[1],y2]
                    _pos =_pos if outLine(_obj) else [ xd-dis1[1] , yd+dis1[0]]
                    aMover["ori"].append(tuple([xd,yd]))
                    aMover["nuevo"].append(tuple(_pos))    
            bloques, ban =[],False
            for i in range(len(_pol)):
                if _pol[i] in aMover["ori"]:
                    index =  aMover["ori"].index(_pol[i])
                    _pol[i]=aMover["nuevo"][index]
                    if not ban:
                        bloques.append({"ini":i,"fin":None})
                        ban=True
                    else:
                        pass
                elif ban:
                    bloques[-1]["fin"]=i-1
                    ban=False
            if len(bloques)>0:
                if bloques[-1]["fin"] == None:
                    bloques.pop()
                bloques.reverse()
                for b in bloques:
                    for i in range(b["fin"]+_s.vtx,b["fin"],-1):
                        try:
                            _pol.pop(i)
                        except Exception as e:
                            pass
                    for i in range(b["ini"]-1,b["ini"]-_s.vtx-1,-1):
                        try:                    
                            _pol.pop(i)
                        except Exception as e:
                            pass

            res.insertRow([nP,_pol])
        print("*-*-*-*-*-*-*-*-*-*-*-*   tmp/resultado    *-*-*-*-*-*-*-*-*-*-*-*")
        with sC("ori",["OID@","SHAPE@AREA"]) as area1:
            with sC("tmp/resultado",["OID@","SHAPE@AREA"]) as area2:
                return  [[a1, a2] for a1,a2 in zip(area1,area2)]


    def crearFeat(_s,_dat,_tip,_proc):
        id=1
        _file = '%s%s' % (_s._nom,_proc)
        cFc(_s._dSet,_file,_tip,None,"DISABLED","DISABLED",_s.sPt)
        aF(_file,"numPol","SHORT")
        with iC("tmp/%s" %_file,["SHAPE@","OID@","numPol"]) as i:
            for p in _s.gK():
                for _d in _dat[p-1]:
                    i.insertRow([_d,id,p])
                    id += 1
        return _file
    

# class Redux_vtx:
#     def __init__(self,d):
#         self._dat = d
#         self._cant = len(d)
#     def run(s):
#         return {"one2one":[[s._dat.pop(i) for i in range(1,int(s._cant/2))],s._dat],
#                     "porPend":lambda aux: "Por pendiente %s" %aux}
        
        
    
        #def eval(m1,m2):
        #     dif = m1-m2
        #     return True if dif<0.8 and dif>-0.8 else False
        # def muere(p,xy):
        #     _s.m = getPend(xy[0],xy[1])
        #     return p
        # def porPend(aux,_i):
        #     _s.m = getPend(aux[0],aux[1])
        #     _elim = [muere(j,[aux[j],aux[j+1]]) for j in range(1,_canC-2) if eval(abs(_s.m),abs(getPend(aux[j],aux[j+1])))]
        #     [_geomTmp[_i].pop(k) for k in reversed(_elim)]

        # _newPol,_geomTmp = [],[_s.getGeom()]
        # if _t!=0:
        #     print("[info] Suavizando polígono por el metodo %s" % "Uno y Uno" if _t==1 else "Pendientes similares")
        #     for i in range(len(_geomTmp)):
        #         _canC = len(_geomTmp[i])
        #         unoYuno(i) if _t==1 else porPend(_geomTmp[i],i)
        # _newPol.append(_geomTmp)
        # return _newPol