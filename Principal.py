# -*- coding: utf-8 -*-
from clases import Convertir as conv
import sys
from time import time as t
from arcpy import env
from arcpy.da import UpdateCursor as uC
from arcpy.management import CalculateField as cF

def principal(_gdb,_fCls,dist,_vtx ):
	ini = t()
	if _gdb[-3:]=="gdb":
		env.workspace=_gdb
		env.overwriteOutput = True
		cF(_fCls,"id_tmp","int('1')","PYTHON",None,"SHORT")
		i = 1
		with uC(_fCls,["OBJECTID","id_tmp"]) as reIndex:
			for  row in  reIndex:
				row[1] = i
				reIndex.updateRow(row)
				i +=1
		_nom = _fCls.split("/")[1] if _fCls.replace("\\","/").find("/") > -1 else _fCls		
		_shp_ = conv(_nom,int(dist)/2,int(_vtx),_gdb,_fCls)

		#while _simp>0 and  len(_shp_.getGeom())>=5000:
			#_shp_.setGeom(rV(_shp_.getGeom()).run()["one2one"][1])
		respTmp = _shp_.Skeletor(_shp_.getNom())
		_shp_._nom_l=_shp_.crearFeat(respTmp["_liniukis"],'POLYLINE',"_Skeletor")
		_shp_._nom_c=_shp_.crearFeat(respTmp["_centra"],'POLYLINE',"_Central")
		print(_shp_.separar())
		print("Tiempo de ejecución: %.3f segundos" % (t()-ini))
	else:
		print("No selecciono una Geodatabase valida.")

if len(sys.argv)==5:
	principal(*sys.argv[1:])
else:
	print("Se deben enviar al menos 4 argumentos")